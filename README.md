# Environment requirements
- OS: macos 
- Ansible version: 2.12.3
- Docker version: 23.0.5
- Python version: 3.10
- Make version: 3.81

Setup requirements: 

Docker on MacOs: https://docs.docker.com/desktop/install/mac-install/

Use brew for the rest.

```bash
brew install python@3.10
brew install ansible@2.12.3
brew install make@3.81
```


# Application overview

## Ping Pong API

### Endpoints
- /ping - Responds with {'pong'}
- /pong - Responds with {'ping'}
- /professional-ping-pong - Responds with {'pong'} 90% of the time
- /amateur-ping-pong - Responds with {'pong'} 70% of the time
- /chance-ping-pong - Responds with {'ping'} 50% of the time and {'pong'} 50% of the time

### Description
This is a simple API to test that the RapidAPI/Mashape API Proxy is working. When you access /ping, the API will return a JSON that contains "pong"

# Application deploymnet
This section describes teh steps to deploy a simple NodeJs api to localhost using Docker and Ansible.

## architecture

In this architecture we have a list of steps to follow to acheive the goal. see the diagram below

```mermaid
graph LR;
    A[Start] --> B[Create Secure Docker Image];
    B --> C[Build Docker Image];
    C --> D[Scan Docker Image];
    D --> E[Deploy Docker Stack for the App];
    E --> F[Test Ping Endpoint];
    F --> G[Clean Old Docker Image];
    G --> H[End];
```

## Create Secure Docker Image


This Dockerfile is designed to build a Docker image for a Node.js application, with a focus on security and efficiency. Let's break down each line to understand its purpose:

#### Line 1: `FROM node:14-bullseye-slim AS build`
- **FROM**: Specifies the base image for the build stage. In this case, it's `node:14-bullseye-slim`, which is a slim version of the Node.js 14 image based on Debian Bullseye.
- **AS build**: Names this stage as `build`. This is useful for multi-stage builds, where you can copy artifacts from one stage to another.

#### Line 2: `RUN apt-get update && apt-get install -y --no-install-recommends dumb-init`
- **RUN**: Executes the specified command.
- **apt-get update**: Updates the package lists for upgrades and new package installations.
- **apt-get install -y --no-install-recommends dumb-init**: Installs `dumb-init` without installing recommended packages. `dumb-init` is a minimal init system that forwards signals to child processes, which is useful for running Node.js applications in Docker containers.

#### Line 3: `WORKDIR /usr/src/app`
- **WORKDIR**: Sets the working directory for any subsequent `RUN`, `CMD`, `ENTRYPOINT`, `COPY`, and `ADD` instructions. In this case, it's set to `/usr/src/app`.

#### Line 4: `COPY package*.json /usr/src/app/`
- **COPY**: Copies new files or directories from `<src>` and adds them to the filesystem of the container at the path `<dest>`. Here, it copies `package.json` and `package-lock.json` (if present) to the working directory.

#### Line 5: `RUN --mount=type=secret,mode=0644,id=npmrc,target=/usr/src/app/.npmrc npm install --only=production`
- **RUN**: Executes the specified command.
- **--mount=type=secret,mode=0644,id=npmrc,target=/usr/src/app/.npmrc**: Mounts a secret file from the host into the container. This is useful for securely passing sensitive information like npm tokens without hardcoding them into the Dockerfile.
- **npm install --only=production**: Installs only the production dependencies specified in `package.json`.

#### Line 6: `FROM node:14-bullseye-slim`
- **FROM**: Specifies the base image for the next stage. This time, it's the same as the build stage, but it starts a new stage for the final image.

#### Line 7: `ENV NODE_ENV production`
- **ENV**: Sets an environment variable. Here, it sets `NODE_ENV` to `production`.

#### Line 8: `COPY --from=build /usr/bin/dumb-init /usr/bin/dumb-init`
- **COPY --from=build**: Copies files from a previous stage (`build` in this case) into the current stage. It copies `dumb-init` from the build stage to the final image.

#### Line 9: `USER node`
- **USER**: Sets the user (and optionally the group) to use when running the image and for any `RUN`, `CMD`, and `ENTRYPOINT` instructions that follow in the Dockerfile. Here, it sets the user to `node`.

#### Line 10: `WORKDIR /usr/src/app`
- **WORKDIR**: Sets the working directory for any subsequent `RUN`, `CMD`, `ENTRYPOINT`, `COPY`, and `ADD` instructions. It's set to `/usr/src/app`.

#### Line 11: `COPY --chown=node:node --from=build /usr/src/app/node_modules /usr/src/app/node_modules`
- **COPY --chown=node:node --from=build**: Copies the `node_modules` directory from the build stage to the final image, changing the ownership to the `node` user.

#### Line 12: `COPY --chown=node:node . /usr/src/app`
- **COPY --chown=node:node**: Copies the current directory (where the Dockerfile is located) into the container, changing the ownership to the `node` user. This includes your application code and any other files in the current directory.

#### Line 13: `EXPOSE 3000`
- **EXPOSE**: Informs Docker that the container listens on the specified network ports at runtime. Here, it's set to `3000`.

#### Line 14: `CMD ["dumb-init", "node", "server.js"]`
- **CMD**: Provides defaults for an executing container. In this case, it specifies the command to run when the container starts, which is `dumb-init`, followed by `node server.js`. This starts the Node.js application using `dumb-init` as the init system.
 

## Deployment playbook

This Ansible playbook is designed to deploy a Docker stack for a Node.js application named `ping-pong-api`, test the `/ping` endpoint, and assert that the response body contains "pong". The playbook is targeted at a host named `localserver` and does not gather facts about the host. Let's break down each task to understand its purpose:

### Overview of Tasks

1. **Install Docker SDK for Python**: Installs the Docker SDK for Python and the `jsondiff` package.
2. **Deploy Docker Stack**: Deploys a Docker stack named `ping-pong-api` using Docker Compose.
3. **Test /ping endpoint**: Sends a request to the `/ping` endpoint and checks the response.
4. **Show response content**: Prints the content of the response received from the `/ping` endpoint.
5. **Assert response body contains "pong"**: Checks if the response body contains the string "pong".


#### 1. Install Docker SDK for Python

- **name**: Specifies the name of the task.
- **ansible.builtin.pip**: Uses the `pip` module to install Python packages.
- **name**: Lists the packages to install, which are `docker` and `jsondiff`.
- **state**: Ensures the specified packages are installed.

#### 2. Deploy Docker Stack

- **name**: Specifies the name of the task.
- **ansible.builtin.docker_stack**: Uses the `docker_stack` module to deploy a Docker stack.
- **name**: Specifies the name of the Docker stack.
- **compose**: Defines the Docker Compose configuration.
- **version**: Specifies the version of the Docker Compose file format.
- **services**: Defines the services to be deployed.
- **ping-pong-api**: The name of the service.
- **image**: Specifies the Docker image to use for the service. The image name is dynamically set using the `image_name` variable.
- **ports**: Maps the container's port 3000 to the host's port 3000.
- **deploy**: Specifies deployment options.
- **replicas**: Sets the number of replicas (instances) of the service to deploy.

#### 3. Test /ping endpoint


- **name**: Specifies the name of the task.
- **ansible.builtin.uri**: Uses the `uri` module to make a request to a URI.
- **url**: Specifies the URL to request.
- **status_code**: Expects a 200 status code in the response.
- **return_content**: Indicates that the content of the response should be returned.
- **register**: Stores the response in a variable named `response`.

#### 4. Show response content

- **name**: Specifies the name of the task.
- **ansible.builtin.debug**: Uses the `debug` module to print the value of a variable.
- **var**: Specifies the variable to print, which is `response`.

#### 5. Assert response body contains "pong"


- **name**: Specifies the name of the task.
- **ansible.builtin.assert**: Uses the `assert` module to check a condition.
- **that**: Specifies the condition to check, which is that the string "pong" is in the response content.
- **fail_msg**: The message to display if the assertion fails.
- **success_msg**: The message to display if the assertion succeeds.

# Pipeline process (Build/Scan/Deploy/Clean)


This Makefile is designed to automate the process of building, scanning, deploying, and cleaning up a Docker image for a Node.js application named `ping-poing-api`. Let's break down each command to understand its purpose and usefulness in a pipeline:

## Variables


- **UMAGE_TAG**: Sets the default tag for the Docker image to `0.4`. The `?=` operator means that the variable will only be set if it's not already defined. This is useful for versioning and ensuring consistency across builds.
- **IMAGE_NAME**: Constructs the full name of the Docker image using the `UMAGE_TAG`. This is crucial for identifying the image in Docker commands and for deployment.
- **DOCKER_FILE**: Specifies the Dockerfile to use for building the image. This allows for flexibility in specifying different Dockerfiles if needed.
- **ANSIBLE_PLAYBOOK**: Path to the Ansible playbook used for deployment. This is essential for automating the deployment process.
- **ANSIBLE_INVENTORY**: Path to the Ansible inventory file used for deployment. This specifies where Ansible should look for the hosts to deploy to.
- **RIVY_DB_PATH**: Path to the Trivy database. This is necessary for Trivy to store its database of vulnerabilities.
- **TRIVY_REPORT_PATH**: Path to the Trivy vulnerability report. This specifies where the vulnerability report should be saved.

## Build Docker Image


- **build**: This target builds the Docker image using the specified `IMAGE_NAME` and `DOCKER_FILE`. It's useful in a pipeline for creating a new version of the application image.

## Scan Docker Image for Vulnerabilities


- **scan**: This target scans the Docker image for vulnerabilities using Trivy. It's crucial in a pipeline for ensuring the security of the application image before deployment.

## Deploy Docker Image to localhost using Ansible


- **deploy**: This target deploys the Docker image to localhost using Ansible. It's essential in a pipeline for automating the deployment process, ensuring consistency and reducing manual errors.

## Clean


- **clean**: This target removes all Docker images tagged with `ping-poing-api` except for the one specified by `UMAGE_TAG`. It's useful in a pipeline for cleaning up old images to save disk space and ensure that only the latest or specified versions are available.

## Default Task


- **all**: This is the default task that runs when you execute `make` without specifying a target. It runs the `build`, `scan`, `deploy`, and `clean` tasks in sequence. This is useful in a pipeline for automating the entire process from building to cleaning up, ensuring that each step is completed successfully before moving on to the next.

# Run the deployment

To run the deployment, just run the following command:

```bash
 make all
 ```

Sample output:

```bash
docker build -t ping-poing-api:0.4 -f Dockerfile .
#1 [internal] load build definition from Dockerfile
#1 transferring dockerfile:
#1 transferring dockerfile: 622B 0.1s done
#1 DONE 0.1s

#2 [internal] load .dockerignore
#2 transferring context: 242B 0.0s done
#2 DONE 0.2s

#3 [internal] load metadata for docker.io/library/node:14-bullseye-slim
#3 DONE 1.1s

#4 [build 1/5] FROM docker.io/library/node:14-bullseye-slim@sha256:0f5b374fae506741ff14db84daff2937ae788e88fb48a6c66d15de5ee808ccd3
#4 DONE 0.0s

#5 [internal] load build context
#5 transferring context: 586B 0.0s done
#5 DONE 0.1s

#6 [stage-1 3/5] WORKDIR /usr/src/app
#6 CACHED

#7 [build 3/5] WORKDIR /usr/src/app
#7 CACHED

#8 [build 4/5] COPY package*.json /usr/src/app/
#8 CACHED

#9 [build 5/5] RUN --mount=type=secret,mode=0644,id=npmrc,target=/usr/src/app/.npmrc npm install --only=production
#9 CACHED

#10 [stage-1 2/5] COPY --from=build /usr/bin/dumb-init /usr/bin/dumb-init
#10 CACHED

#11 [build 2/5] RUN apt-get update && apt-get install -y --no-install-recommends dumb-init
#11 CACHED

#12 [stage-1 4/5] COPY --chown=node:node --from=build /usr/src/app/node_modules /usr/src/app/node_modules
#12 CACHED

#13 [stage-1 5/5] COPY --chown=node:node . /usr/src/app
#13 DONE 0.1s

#14 exporting to image
#14 exporting layers
#14 exporting layers 0.1s done
#14 writing image sha256:4ba223b324a64cd310df203a3039affe1d23a8daf9d51d88645892d5d2232aa1 done
#14 naming to docker.io/library/ping-poing-api:0.4 0.0s done
#14 DONE 0.1s
docker run --rm -v /Users/mojave/OhmyOps/spare/devops-test/ping-pong-api/.cache/trivy/db:/opt  -v /Users/mojave/OhmyOps/spare/devops-test/ping-pong-api/report:/tmp/ -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy --cache-dir /opt/trivy image ping-poing-api:0.4 --format template --template "@contrib/html.tpl" --output /tmp/trivy_report.html
2024-03-28T04:05:17.781Z	[34mINFO[0m	Vulnerability scanning is enabled
2024-03-28T04:05:17.782Z	[34mINFO[0m	Secret scanning is enabled
2024-03-28T04:05:17.782Z	[34mINFO[0m	If your scanning is slow, please try '--scanners vuln' to disable secret scanning
2024-03-28T04:05:17.782Z	[34mINFO[0m	Please see also https://aquasecurity.github.io/trivy/v0.50/docs/scanner/secret/#recommendation for faster secret detection
2024-03-28T04:06:00.167Z	[34mINFO[0m	Detected OS: debian
2024-03-28T04:06:00.167Z	[34mINFO[0m	Detecting Debian vulnerabilities...
2024-03-28T04:06:00.413Z	[34mINFO[0m	Number of language-specific files: 1
2024-03-28T04:06:00.413Z	[34mINFO[0m	Detecting node-pkg vulnerabilities...
ansible-playbook -i deployment/inventory deployment/deploy.yml -e "image_name=ping-poing-api:0.4"

PLAY [Deploy Docker Stack] *****************************************************

TASK [Install Docker SDK for Python] *******************************************
[WARNING]: Platform darwin on host localserver is using the discovered Python
interpreter at /usr/local/bin/python3.10, but future installation of another
Python interpreter could change the meaning of that path. See
https://docs.ansible.com/ansible-
core/2.12/reference_appendices/interpreter_discovery.html for more information.
ok: [localserver]

TASK [Deploy Docker Stack] *****************************************************
ok: [localserver]

TASK [Test /ping endpoint] *****************************************************
ok: [localserver]

TASK [Show response content] ***************************************************
ok: [localserver] => {
    "response": {
        "changed": false,
        "connection": "close",
        "content": "\"pong\"",
        "content_length": "6",
        "content_type": "application/json; charset=utf-8",
        "cookies": {},
        "cookies_string": "",
        "date": "Thu, 28 Mar 2024 04:06:40 GMT",
        "elapsed": 0,
        "etag": "W/\"6-uBnwlsJiQ3kuZAgKKWB4aV5ugdE\"",
        "failed": false,
        "json": "pong",
        "msg": "OK (6 bytes)",
        "redirected": false,
        "status": 200,
        "url": "http://localhost:3000/ping",
        "x_powered_by": "Express"
    }
}

TASK [Assert response body contains "pong"] ************************************
ok: [localserver] => {
    "changed": false,
    "msg": "Test passed: 'pong' found in response body"
}

PLAY RECAP *********************************************************************
localserver                : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

docker images ping-poing-api --format "{{.Tag}}" | grep -v "0.4" | xargs -I {} docker rmi -f ping-poing-api:{} 

```

docker service overview:
![docker service](images/docker-service.png)

Trivy report overview:
![trivy report](images/trivy-report.png)

application /ping: 

![app](images/app.png)