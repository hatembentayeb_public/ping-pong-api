# Variables
UMAGE_TAG ?= 0.4
IMAGE_NAME ?= ping-poing-api:$(UMAGE_TAG)
DOCKER_FILE ?= Dockerfile
ANSIBLE_PLAYBOOK ?= deployment/deploy.yml
ANSIBLE_INVENTORY ?= deployment/inventory
RIVY_DB_PATH ?= /opt/trivy
TRIVY_REPORT_PATH ?= trivy_report.html

# Build Docker image
build:
	docker build -t $(IMAGE_NAME) -f $(DOCKER_FILE) .

# Scan Docker image for vulnerabilities
scan:
	mkdir -p .cache/trivy/db || true
	mkdir report || true
	docker run --rm -v $(PWD)/.cache/trivy/db:/opt  -v $(PWD)/report:/tmp/ -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy --cache-dir /opt/trivy image $(IMAGE_NAME) --format template --template "@contrib/html.tpl" --output /tmp/$(TRIVY_REPORT_PATH)

# Deploy Docker image to localhost using Ansible
deploy:
	ansible-playbook -i $(ANSIBLE_INVENTORY) $(ANSIBLE_PLAYBOOK) -e "image_name=$(IMAGE_NAME)"

clean:
	docker images ping-poing-api --format "{{.Tag}}" | grep -v "$(UMAGE_TAG)" | xargs -I {} docker rmi -f ping-poing-api:{} 
# Default task
all: build scan deploy clean
